import React from "react";
import { View, Text, FlatList } from "react-native";
import style from "./styles";

import Post from "../../components/Post";
import feed from "../../data/feed";
import { SIZES } from "../../constants";

const ITEM_HEIGHT = SIZES.height * 0.9 + SIZES.padding;

export default function index() {
  return (
    <View style={style.container}>
      <FlatList
        data={feed}
        keyExtractor={(post) => String(post.id)}
        snapToInterval={ITEM_HEIGHT}
        decelerationRate={0}
        renderItem={({ item }) => <Post post={item} />}
      />
    </View>
  );
}
