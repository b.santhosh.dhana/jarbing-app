import { StyleSheet } from "react-native";
import { COLORS } from "../../constants";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.black,
  },
});

export default style;
