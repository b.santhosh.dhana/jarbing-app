import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: SIZES.padding,
  },
  image: {
    width: SIZES.width,
    height: SIZES.height * 0.9,
  },

  header: {
    position: "absolute",
    flexDirection: "row",
    alignItems: "center",
    height: 50,
    width: 200,
    top: SIZES.padding2 * 3,
    paddingHorizontal: SIZES.padding2 * 2,
  },
  profilePic: {
    width: 40,
    height: 40,
    borderRadius: 10,
    marginRight: SIZES.padding,
  },
  username: {
    fontFamily: "RobotoBold",
    fontSize: 24,
    color: COLORS.white,
  },
  time: {
    color: COLORS.white,
    lineHeight: 16,
  },
});

export default styles;
