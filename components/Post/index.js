import React from "react";
import { View, Text, Image } from "react-native";
import styles from "./styles";

const Post = ({ post }) => {
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: post.image }}
        style={styles.image}
        resizeMode="cover"
      />
      {/* header */}
      <View style={styles.header}>
        <Image style={styles.profilePic} source={{ uri: post.image }} />
        <View>
          <Text style={styles.username}>{post.userName}</Text>
          <Text style={styles.time}>Today</Text>
        </View>
      </View>
      {/* pagination */}
    </View>
  );
};

export default Post;
