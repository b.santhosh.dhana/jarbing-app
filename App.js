import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { useFonts } from "expo-font";

import fonts from "./constants/fonts";

import HomeScreen from "./screens/Home";

export default function App() {
  const [loaded] = useFonts(fonts);

  if (!loaded) {
    return <Text>Loading..</Text>;
  }

  return (
    <View style={styles.container}>
      <StatusBar style="" />
      <HomeScreen />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
