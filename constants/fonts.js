const RobotoThin = require("../assets/fonts/Roboto-Thin.ttf");
const RobotoLight = require("../assets/fonts/Roboto-Light.ttf");
const RobotoRegular = require("../assets/fonts/Roboto-Regular.ttf");
const RobotoMedium = require("../assets/fonts/Roboto-Medium.ttf");
const RobotoBold = require("../assets/fonts/Roboto-Bold.ttf");
const RobotoBlack = require("../assets/fonts/Roboto-Black.ttf");

export default {
  RobotoThin,
  RobotoLight,
  RobotoRegular,
  RobotoMedium,
  RobotoBold,
  RobotoBlack,
};
