import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("screen");

export const COLORS = {
  primary: "#00CD6B",
  black: "#000000",
  white: "#fff",
};

export const SIZES = {
  height,
  width,
  padding: 10,
  padding2: 12,
};
